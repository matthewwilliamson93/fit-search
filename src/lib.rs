use std::cmp::{Ordering, PartialOrd};
use std::convert::Into;
use std::ops::Sub;

use conv::prelude::*;

fn is_sorted<T: PartialOrd>(arr: &[T]) -> bool {
    arr.windows(2).all(|w| w[0] <= w[1])
}

pub fn binary_search<T: PartialOrd>(arr: &[T], key: T) -> Option<usize> {
    debug_assert!(is_sorted(arr));

    let mut low = 0;
    let mut high = arr.len() - 1;

    while low < high {
        let middle = ((low + high) / 2) as usize;

        match arr[middle].partial_cmp(&key).unwrap() {
            Ordering::Less => {
                low = middle + 1;
            }
            Ordering::Equal => return Some(middle),
            Ordering::Greater => {
                high = middle - 1;
            }
        }
    }

    if key == arr[low] {
        Some(low)
    } else {
        None
    }
}

pub fn linear_search<T>(arr: &[T], key: T) -> Option<usize>
where
    T: Copy + PartialOrd + Sub + Into<f64> + ValueFrom<usize>,
    f64: std::convert::From<<T as std::ops::Sub>::Output>,
{
    debug_assert!(is_sorted(arr));

    let mut low = 0;
    let mut high = arr.len() - 1;

    while arr[low] != arr[high] && arr[low] <= key && key <= arr[high] {
        let y0 = (high - low) as f64;
        let m1: f64 = (arr[high] - arr[low]).into();
        let y1: f64 = (key - arr[low]).into();
        let x1 = low + (y1 / m1 * y0) as usize;

        match arr[x1].partial_cmp(&key).unwrap() {
            Ordering::Less => {
                low = x1 + 1;
            }
            Ordering::Equal => return Some(x1),
            Ordering::Greater => {
                high = x1 - 1;
            }
        }
    }

    if key == arr[low] {
        Some(low)
    } else {
        None
    }
}

pub fn hybrid_search<T>(arr: &[T], key: T) -> Option<usize>
where
    T: Copy + PartialOrd + Sub + Into<f64> + ValueFrom<usize>,
    f64: std::convert::From<<T as std::ops::Sub>::Output>,
{
    debug_assert!(is_sorted(arr));

    let mut low = 0;
    let mut high = arr.len() - 1;

    while low < high {
        // Binary
        let middle = ((low + high) / 2) as usize;
        match arr[middle].partial_cmp(&key).unwrap() {
            Ordering::Less => {
                low = middle + 1;
            }
            Ordering::Equal => return Some(middle),
            Ordering::Greater => {
                high = middle - 1;
            }
        }
        // Linear Fit
        if arr[low] == arr[high] || key < arr[low] || arr[high] < key {
            break;
        }

        let y0 = (high - low) as f64;
        let m1: f64 = (arr[high] - arr[low]).into();
        let y1: f64 = (key - arr[low]).into();
        let x1 = low + (y1 / m1 * y0) as usize;

        match arr[x1].partial_cmp(&key).unwrap() {
            Ordering::Less => {
                low = x1 + 1;
            }
            Ordering::Equal => return Some(x1),
            Ordering::Greater => {
                high = x1 - 1;
            }
        }
    }

    if key == arr[low] {
        Some(low)
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn binary_search_0() {
        assert_eq!(binary_search(&[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 3), Some(3));
    }

    #[test]
    fn binary_search_1() {
        assert_eq!(binary_search(&[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 3), Some(3));
    }

    #[test]
    fn binary_search_2() {
        assert_eq!(
            binary_search(&[0, 1, 2, 3, 3, 4, 5, 6, 7, 8, 9], 4),
            Some(5)
        );
    }

    #[test]
    fn binary_search_3() {
        assert_eq!(binary_search(&[0, 1, 3, 4, 5, 6, 7, 8, 9], 3), Some(2));
    }

    #[test]
    fn binary_search_4() {
        assert_eq!(binary_search(&[0, 1, 2, 3, 4, 6, 7, 8, 9], 5), None);
    }

    #[test]
    fn linear_search_0() {
        assert_eq!(linear_search(&[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 3), Some(3));
    }

    #[test]
    fn linear_search_1() {
        assert_eq!(linear_search(&[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 3), Some(3));
    }

    #[test]
    fn linear_search_2() {
        assert_eq!(
            linear_search(&[0, 1, 2, 3, 3, 4, 5, 6, 7, 8, 9], 4),
            Some(5)
        );
    }

    #[test]
    fn linear_search_3() {
        assert_eq!(linear_search(&[0, 1, 3, 4, 5, 6, 7, 8, 9], 3), Some(2));
    }

    #[test]
    fn linear_search_4() {
        assert_eq!(linear_search(&[0, 1, 2, 3, 4, 6, 7, 8, 9], 5), None);
    }

    #[test]
    fn linear_search_5() {
        assert_eq!(linear_search(&[0.0, 1.0], 5.0), None);
    }

    #[test]
    fn hybrid_search_0() {
        assert_eq!(hybrid_search(&[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 3), Some(3));
    }

    #[test]
    fn hybrid_search_1() {
        assert_eq!(hybrid_search(&[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 3), Some(3));
    }

    #[test]
    fn hybrid_search_2() {
        assert_eq!(
            hybrid_search(&[0, 1, 2, 3, 3, 4, 5, 6, 7, 8, 9], 4),
            Some(5)
        );
    }

    #[test]
    fn hybrid_search_3() {
        assert_eq!(hybrid_search(&[0, 1, 3, 4, 5, 6, 7, 8, 9], 3), Some(2));
    }

    #[test]
    fn hybrid_search_4() {
        assert_eq!(hybrid_search(&[0, 1, 2, 3, 4, 6, 7, 8, 9], 5), None);
    }

    #[test]
    fn hybrid_search_5() {
        assert_eq!(hybrid_search(&[0.0, 1.0], 5.0), None);
    }
}
