use fit_search;

#[macro_use]
extern crate criterion;

use criterion::Criterion;
use criterion::ParameterizedBenchmark;

use rand::distributions::{Distribution, Normal, Uniform};
use rand::Rng;

fn random_entry<T: Copy>(vec: &Vec<T>) -> T {
    let mut rng = rand::thread_rng();
    vec[rng.gen_range(0, vec.len())]
}

fn make_normal_list(size: usize) -> Vec<f64> {
    let mut rng = rand::thread_rng();
    let normal = Normal::new(1.0, 0.5);
    let mut vec: Vec<f64> = normal.sample_iter(&mut rng).take(size).collect();
    vec.sort_by(|a, b| a.partial_cmp(b).unwrap());
    vec
}

fn make_uniform_list(size: usize) -> Vec<i32> {
    let mut rng = rand::thread_rng();
    let uniform = Uniform::from(0..10000);
    let mut vec: Vec<i32> = uniform.sample_iter(&mut rng).take(size).collect();
    vec.sort();
    vec
}

fn bench_normal(c: &mut Criterion) {
    c.bench(
        "Fit Search",
        ParameterizedBenchmark::new(
            "Binary",
            |b, i| b.iter(|| fit_search::binary_search(i, random_entry(i))),
            vec![make_normal_list(100), make_normal_list(1000)],
        )
        .with_function("Linear", |b, i| {
            b.iter(|| fit_search::linear_search(i, random_entry(i)))
        }),
    );
}

fn bench_uniform(c: &mut Criterion) {
    c.bench(
        "Fit Search",
        ParameterizedBenchmark::new(
            "Binary",
            |b, i| b.iter(|| fit_search::binary_search(i, random_entry(i))),
            vec![make_uniform_list(100), make_uniform_list(1000)],
        )
        .with_function("Linear", |b, i| {
            b.iter(|| fit_search::linear_search(i, random_entry(i)))
        }),
    );
}

criterion_group!(benches, bench_normal, bench_uniform);
criterion_main!(benches);
