# fit-search

A demo to play around with fit search based algorithms.

## Build / Test / Benchmark

```
cargo build
cargo test
cargo bench
```
